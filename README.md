# React Native Course

All projects and apps within this repo were created as a result of the [React Native - The Practical Guide [2024]](https://www.udemy.com/course/react-native-the-practical-guide/) Course by Academind

## Course Goals App
The project called `my-react-native-project` is the result of Chapter 2 and includes the React Native Basics

## Min Game App
The project called `starting-project` is the result of Chapter 4 and includes a deep dive into components, layouts and styling

# React Native Commands

Install all needed dependencies
```
yarn install
```

Launch metro bundler
```
yarn start
```